
(function (ContentFetcher) {
  'use strict';
  var NO_CONTENT_MESSAGE = '&#x26a0; Content could not be loaded, please try again later';

  var accordions = document.getElementsByClassName('accordion');
  for(var i =0; i < accordions.length; i++) {
    var accordionData = setupAccordion(accordions[i]);
    accordionData.accordionOptions[0].click();
  }

  /**
   * Add the given content
   *
   * @param accordionTitle
   * @param contentHTML
   */
  function appendContentHTML(accordionTitle, contentHTML) {
    var fullHTML = '<div class="accordion-content">' + contentHTML + '</div>';
    accordionTitle.insertAdjacentHTML('afterend', fullHTML);
  }

  /**
   * @param accordionTitle
   * @param accordionData
   */
  function handleAccordionChange(accordionTitle, accordionData) {
    var accordionOption = accordionData.accordionOptions.indexOf(accordionTitle);
    if (accordionOption > -1 && !accordionData.loaded[accordionOption]) {
      ContentFetcher.fetchContent(accordionOption, function (contentHTML) {
        appendContentHTML(accordionTitle, contentHTML);
        accordionData.loaded[accordionOption] = true;
      }, function() {
        appendContentHTML(accordionTitle, NO_CONTENT_MESSAGE);
        accordionData.loaded[accordionOption] = true;
      });
    }
  }

  /**
   * @param accordion
   */
  function setupAccordion(accordion) {
    var accordionData = {
      loaded: []
    };
    var accordionOptionNodeList = accordion.getElementsByClassName('accordion-title');
    accordionData.accordionOptions = Array.prototype.slice.call(accordionOptionNodeList);
    accordion.addEventListener('click', function (clickEvent) {
      handleAccordionChange(clickEvent.target, accordionData);
    });
    return accordionData;
  }

})(App.ContentFetcher);