var App = {};
(function (_App) {
  'use strict';
  function fetchContent(contentId, actionCallback, errorCallback) {
    var request = new XMLHttpRequest();
    request.open('GET', './content/content-' + contentId + '.html', true);
    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        var resp = request.responseText;
        actionCallback(resp);
      } else {
        errorCallback(resp)
      }
    };

    request.onerror = function (error) {
      errorCallback(error)
    };

    request.send();
  }

  _App.ContentFetcher = {
    fetchContent: fetchContent
  }

})(App);